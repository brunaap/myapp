import { DatabaseService, Dev } from './../../services/database.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-developers',
  templateUrl: './developers.page.html',
  styleUrls: ['./developers.page.scss'],
})
export class DevelopersPage implements OnInit {
  developers: Dev[] = [];
  products: Observable<any[]>;
  developer = {};
  product = {};
  selectedView = 'devs';

  constructor(private db: DatabaseService) { }

  ngOnInit() {
    this.db.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.db.getDevs().subscribe(devs => {
          this.developers = devs;
        // tslint:disable-next-line: semicolon
        })
        this.products = this.db.getProducts();
      }
    });
  }

  addDeveloper() {
    // tslint:disable-next-line: no-string-literal
    let skills = this.developer['skills'].split(',');
    skills = skills.map(skill => skill.trim());

    // tslint:disable-next-line: no-string-literal
    this.db.addDeveloper(this.developers['name'], skills, this.developer['img'])
    .then(_ => {
      this.developer = {};
    });
  }

  addProduct() {
    // tslint:disable-next-line: no-string-literal
    this.db.addProduct(this.product['name'], this.product['creator'])
    .then(_ => {
      this.product = {};
    });
  }

}
