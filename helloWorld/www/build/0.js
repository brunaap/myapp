webpackJsonp([0],{

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThirdPageModule", function() { return ThirdPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__third__ = __webpack_require__(278);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ThirdPageModule = /** @class */ (function () {
    function ThirdPageModule() {
    }
    ThirdPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__third__["a" /* ThirdPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__third__["a" /* ThirdPage */])
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__third__["a" /* ThirdPage */]
            ]
        })
    ], ThirdPageModule);
    return ThirdPageModule;
}());

//# sourceMappingURL=third.module.js.map

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThirdPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ThirdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ThirdPage = /** @class */ (function () {
    function ThirdPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.message = this.navParams.get('message'); // nome enviado por outra pagina
        //alert(this.message);
    }
    ThirdPage.prototype.navigateToPage = function (n) {
        if (n == -1)
            this.navCtrl.pop();
        if (n == 0)
            this.navCtrl.push('NotesPage');
        if (n == 1)
            this.navCtrl.push('HomePage');
        if (n == 2)
            this.navCtrl.push('SecondPage'); // nome da classe na pagina.ts
        if (n == 3)
            this.navCtrl.push('ThirdPage', {
                message: "Hello from the HomePage"
            });
    };
    ThirdPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ThirdPage');
    };
    ThirdPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-third',template:/*ion-inline-start:"/home/lapti/helloWorld/src/pages/third/third.html"*/'<!--\n  Generated template for the ThirdPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>Animes</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  Animes que eu vi, estou vendo ou pretendo ver.\n  <p>\n      <button ion-button color="primary" block outline (click)="navigateToPage(-1)">voltar</button>\n  </p>\n  \n\n</ion-content>\n'/*ion-inline-end:"/home/lapti/helloWorld/src/pages/third/third.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], ThirdPage);
    return ThirdPage;
}());

//# sourceMappingURL=third.js.map

/***/ })

});
//# sourceMappingURL=0.js.map