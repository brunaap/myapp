webpackJsonp([5],{

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FourthPageModule", function() { return FourthPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fourth__ = __webpack_require__(273);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FourthPageModule = /** @class */ (function () {
    function FourthPageModule() {
    }
    FourthPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fourth__["a" /* FourthPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__fourth__["a" /* FourthPage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__fourth__["a" /* FourthPage */],
            ]
        })
    ], FourthPageModule);
    return FourthPageModule;
}());

//# sourceMappingURL=fourth.module.js.map

/***/ }),

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FourthPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the FourthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FourthPage = /** @class */ (function () {
    function FourthPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FourthPage.prototype.navigateToPage = function (n) {
        if (n == -1)
            this.navCtrl.pop();
        if (n == 2)
            this.navCtrl.push('HomePage');
        if (n == 2)
            this.navCtrl.push('SecondPage'); // nome da classe na pagina.ts
        if (n == 3)
            this.navCtrl.push('ThirdPage', {
                message: "Hello from the HomePage"
            });
    };
    FourthPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FourthPage');
    };
    FourthPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fourth',template:/*ion-inline-start:"/home/lapti/helloWorld/src/pages/fourth/fourth.html"*/'<!--\n  Generated template for the FourthPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="secondary">\n    <ion-title>Magic</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  Decks de magic e cartas que tenho ou pretendo montar, ou simplesmente acho bonitas.\n  <p>\n      Site para cards de Magic: <a href="https://www.manafix.net/">Mana Fix</a>\n  </p>\n  <p>\n    <button ion-button color="secondary" block outline (click)=navigateToPage(-1)>voltar</button>\n  </p>\n</ion-content>\n'/*ion-inline-end:"/home/lapti/helloWorld/src/pages/fourth/fourth.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], FourthPage);
    return FourthPage;
}());

//# sourceMappingURL=fourth.js.map

/***/ })

});
//# sourceMappingURL=5.js.map