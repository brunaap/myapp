import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FourthPage } from './fourth';

@NgModule({
  declarations: [
    FourthPage,
  ],
  imports: [
    IonicPageModule.forChild(FourthPage),
  ],
  exports: [
    FourthPage,
  ]
})
export class FourthPageModule {}
