import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FourthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fourth',
  templateUrl: 'fourth.html',
})
export class FourthPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  navigateToPage(n): void {
    if( n == -1 ) // volta uma página
      this.navCtrl.pop();
    if( n == 2 )
      this.navCtrl.push('HomePage');
    if( n == 2 )
      this.navCtrl.push('SecondPage'); // nome da classe na pagina.ts
    if( n == 3 )
      this.navCtrl.push('ThirdPage', {
        message: "Hello from the HomePage"
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FourthPage');
  }

}
