import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  navigateToPage(n): void {
    if( n == -1 ) // volta uma página
      this.navCtrl.pop();
    if( n == 0 )
      this.navCtrl.push('NotesPage');
    if( n == 2 )
      this.navCtrl.push('SecondPage'); // nome da classe na pagina.ts
    if( n == 3 )
      this.navCtrl.push('ThirdPage', {
        message: "Hello from the HomePage"
      });
    if( n == 4 )
      this.navCtrl.push('FourthPage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad fired once, upn page load');
  }

  ionViewWillEnter() {
    console.log('ionicViewWillEnter is fired just as the page is about become active.');
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter fired each time the page has entered.');
  }

  ionViewWillLeave() {
    console.log('ionViewWillLeave is fired each time the page is ABOUT to leave.');
  }

  ionicViewDidLeave() {
    console.log('ionViewDidLeave is fired when the user has left th epage.');
  }

  ionViewUnload() {
    console.log('ionViewWillUnload ran when the page is about to be destroyed.');
  }

}
