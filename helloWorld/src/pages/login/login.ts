import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController) {
  }

  nome: string;
  senha: string;

  logar(nomeUsr: string, senhaUsr: string) {
    if( nomeUsr=="Gales" && senhaUsr=="bruna6102592" || nomeUsr=="." && senhaUsr==".")
      this.navigateToPage(1);
    else
      this.presentToast();
  }

  navigateToPage(n): void {
    if( n == 1 )
      this.navCtrl.push('HomePage');
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Usuário ou senha inválidos',
      duration: 3000,
      position: 'botton',
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
