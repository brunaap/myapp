import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ThirdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-third',
  templateUrl: 'third.html',
})
export class ThirdPage {

  message : string;

  constructor(private navCtrl: NavController, private navParams: NavParams) {
    this.message = this.navParams.get('message'); // nome enviado por outra pagina
    //alert(this.message);
  }

  navigateToPage(n): void {
    if( n == -1 ) // volta uma página
      this.navCtrl.pop();
    if( n == 0 )
      this.navCtrl.push('NotesPage');
    if( n == 1 )
      this.navCtrl.push('HomePage');
    if( n == 2 )
      this.navCtrl.push('SecondPage'); // nome da classe na pagina.ts
    if( n == 3 )
      this.navCtrl.push('ThirdPage', {
        message: "Hello from the HomePage"
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ThirdPage');
  }

}
